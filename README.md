# Lox

Lox is a reputation-based bridge distribution system that provides privacy protection to users and their social graph and is open to all users.
Lox is written in rust and requires `cargo` to test. [Install Rust](https://www.rust-lang.org/tools/install). We used Rust version 1.56.0.

### To use the docker environment to build the package:

```
./build-lox.sh
./run-lox.sh
```

### To run each of the tests used for our experimental results run:

```
cargo test --release -- --nocapture TESTNAME > LOGFILE
```

Where `TESTNAME > LOGFILE` is one of:

```
stats_test_trust_levels  > trust_levels.log
stats_test_invitations > invitations.log
stats_test_percent_blockage_migration_05 > blockage_migration05.log
stats_test_percent_blockage_migration_010 > blockage_migration010.log
stats_test_percent_blockage_migration_20 > blockage_migration20.log
stats_test_percent_blockage_migration_25 > blockage_migration25.log
stats_test_percent_blockage_migration_35 > blockage_migration35.log
stats_test_percent_blockage_migration_40 > blockage_migration40.log
stats_test_percent_blockage_migration_45 > blockage_migration45.log
stats_test_percent_blockage_migration_50 > blockage_migration50.log
stats_test_percent_blockage_migration_55 > blockage_migration55.log
stats_test_percent_blockage_migration_60 > blockage_migration60.log
stats_test_percent_blockage_migration_65 > blockage_migration65.log
stats_test_percent_blockage_migration_70 > blockage_migration70.log
stats_test_percent_blockage_migration_75 > blockage_migration75.log
stats_test_percent_blockage_migration_80 > blockage_migration80.log
stats_test_percent_blockage_migration_85 > blockage_migration85.log
stats_test_percent_blockage_migration_90 > blockage_migration90.log
stats_test_percent_blockage_migration_95 > blockage_migration95.log
stats_test_percent_blockage_migration_100 > blockage_migration100.log
```

Each test outputs results to the specified log file and takes approximately 20-30 hours to run. However, this can be improved
by passing the `fast` feature. Using this feature, our tests are run for 100
users instead of 10000 users and will produce results comparable to our
reported results (with larger error margins). To run individual tests with this
flag run:

```
cargo test --release --features=fast -- --nocapture TESTNAME > LOGFILE
```

We have also included the scripts we used to parse the output from each of the Lox tests in the `Parsing-results` directory. For convenience, copy all of the output log files to the `Parsing-results` directory and run `./parse_data.sh`. This is a python script that uses Python 3.8+ and depends on `numpy`, `matplotlib`, and `pandas` which can be installed with `pip3`.

To run all tests in fast mode, output the results to the `Parsing-results` directory, and generate the table (`performance_stats.csv`, our Table 4) and graphs (`Blockage-response-time.pdf` and `core-users.pdf`, our Figures 1 and 2) used in our paper, run:

```
./run_tests_fast
```

This takes 5–6 hours to complete on the 2011-era E7-8870 processor we used for the measurements in the paper.  Newer processors will likely take less time. Note that with the `fast` feature, it is possible that some tests may only yield 0 or 1 data points, in which case you will see `0` for the times and/or stddevs in the resulting `performance_stats.csv` table for those tests.

Note that our implementation is coded such that the reachability certificate expires at 00:00 UTC. A workaround has been included in each test to pause if it is too close to this time so the request won't fail. In reality, if the bucket is still reachable, a user could simply request a new reachability token if their request fails for this reason (a new certificate should be available prior to the outdated certificate expiring).


