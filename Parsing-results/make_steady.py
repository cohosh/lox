#!/usr/bin/env python

import sys
import json
import lzma
import numpy as np

import matplotlib
import matplotlib.pyplot as pyplot
import matplotlib.colors as mcolors


def main():
    set_plot_options()
    buckets = [600, 1200, 1800, 2400, 3000]
    n = 1000000
    ft = []

    pyplot.figure()
    for b in buckets:
        delta = np.arange(1/10,1,.01)
        x = []
        for d in delta:
            p = n*(1.61 + (14.71/d+23.725/d**2)/b) / 86400000
            x.append(p)
        ft.append(x)


    pyplot.plot(delta, ft[0], label='600 Buckets')
    pyplot.plot(delta, ft[1], linestyle='dotted', label='1200 Buckets')
    pyplot.plot(delta, ft[2], linestyle='dashed', label='1800 Buckets')
    pyplot.plot(delta, ft[3], linestyle='dashdot', label='2400 Buckets')
    pyplot.plot(delta, ft[4], label='3000 Buckets')
    pyplot.ylim(bottom=0)

    pyplot.xlabel(r'$\Delta$')
    pyplot.ylabel("Cores / Million Users")
#    pyplot.title("Average Number of Bridge Users for 1 Month Old Bridges November 2021")


    pyplot.legend(loc="upper right")
    pyplot.tight_layout(pad=1)
    pyplot.savefig("core-users.pdf")

def set_plot_options():
    options = {
        #'backend': 'PDF',
        'font.size': 18,
        'figure.figsize': (7,4),
        'figure.dpi': 100.0,
        'axes.grid' : True,
        'axes.xmargin' : 0,
        'axes.grid.axis' : 'y',
        'axes.axisbelow': True,
        'axes.titlesize' : 'medium',
        'axes.labelsize' : 'large',
        'axes.formatter.limits': (-6,6),
        'xtick.labelsize' : 18,#get_tick_font_size_10(),
        'ytick.labelsize' : 18,
        'lines.linewidth' : 2.0,
        'lines.markersize' : 10,
        # turn on the following to embedd fonts; requires latex
        'ps.useafm' : True,
        'pdf.use14corefonts' : True,
        'text.usetex' : False,
    }

    for option_key in options:
        matplotlib.rcParams[option_key] = options[option_key]

    if 'figure.max_num_figures' in matplotlib.rcParams:
        matplotlib.rcParams['figure.max_num_figures'] = 100
    if 'figure.max_open_warning' in matplotlib.rcParams:
        matplotlib.rcParams['figure.max_open_warning'] = 100
    if 'legend.ncol' in matplotlib.rcParams:
        matplotlib.rcParams['legend.ncol'] = 100

if __name__ == "__main__":
    sys.exit(main())
