import sys
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D


def main():

    fig, axs = plt.subplots(1, 3, figsize=(24, 7))
    columns = ["Bridges", "RequestT", "Rtstdev", "ResponseS", "ResponseT",
               "ReTstdev", "ResponseHT", "RHTstdev"]
    df = pd.read_csv("trust_promo"+".csv", usecols=columns)
    bridges = df.Bridges*2*3
    fig.supxlabel('Total Size of Bridge Pool',size=30)
    axs[0].set_ylabel('Response Time (ms)', size=25)
    axs[0].tick_params(axis='x', labelsize=15)                                  
    axs[0].tick_params(axis='y', labelsize=15)     
    axs[0].plot(bridges, df.ResponseT, color='#29135F',
                label='Response Time for Trust Promotion')
    axs[0].fill_between(bridges, df.ResponseT-df.ReTstdev,
                        df.ResponseT+df.ReTstdev, alpha=0.5, edgecolor='#29135F',
                        facecolor='#8967E2')
    axs[1].set_ylabel('Response Size (bytes)', size=25)
    axs[1].tick_params(axis='x', labelsize=15)                                  
    axs[1].tick_params(axis='y', labelsize=15)         
    axs[1].plot(bridges, df.ResponseS, color='#29135F',
                label='Response Size for Trust Promotion')
    axs[2].set_ylabel('Response Handling Time (ms)', size=25)
    axs[2].tick_params(axis='x', labelsize=15)                                  
    axs[2].tick_params(axis='y', labelsize=15)     
    axs[2].plot(bridges, df.ResponseHT, color='#29135F',
                label='Response Handling Time for Trust Promotion')
    axs[2].fill_between(bridges, df.ResponseHT-df.RHTstdev,
                        df.ResponseHT+df.RHTstdev, alpha=0.5, edgecolor='#29135F',
                        facecolor='#8967E2')
    fig. tight_layout(pad=1)
    fig.savefig("TrustPromotion.pdf")
    print("\nDone Trust Promotion Plot.\nOutput to: TrustPromotion.pdf")

def set_plot_options():
      options = {
          'font.size': 12,
          'figure.figsize': (4,2),
          'figure.dpi': 100.0,
          'figure.subplot.left': 0.20,
          'figure.subplot.right': 0.97,
          'figure.subplot.bottom': 0.20,
          'figure.subplot.top': 0.90,
          'grid.color': '0.1',
          'grid.linestyle': ':',
          #'grid.linewidth': 0.5,
          'axes.grid' : True,
          #'axes.grid.axis' : 'y',
          #'axes.axisbelow': True,
          'axes.titlesize' : 'large',
          'axes.labelsize' : 'x-large',
          'axes.formatter.limits': (-4,4),
          'xtick.labelsize' : 20,#get_tick_font_size_10(),
          'ytick.labelsize' : 20,#get_tick_font_size_10(),
          'lines.linewidth' : 2.0,
          'lines.markeredgewidth' : 0.5,
          'lines.markersize' : 10,
      }

      for option_key in options:
          matplotlib.rcParams[option_key] = options[option_key]
      if 'figure.max_num_figures' in matplotlib.rcParams:
          matplotlib.rcParams['figure.max_num_figures'] = 100
      if 'figure.max_open_warning' in matplotlib.rcParams:
          matplotlib.rcParams['figure.max_open_warning'] = 100

if __name__ == "__main__":
    sys.exit(main())
