import sys
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D


def main():

    plt.figure(figsize=(9, 5))
    columns = ["Percent","Bridges", "RequestT", "Rtstdev", "ResponseS", "ResponseT",
               "ReTstdev", "ResponseHT", "RHTstdev"]
    df = pd.read_csv("standard_check"+".csv", usecols=columns)
    df.sort_values(["Percent"], axis=0,ascending=[False], inplace=True)
    bridges = df.Bridges*2*3
    plt.xlabel('Blocked Bridges (%)', size=20)
    plt.ylabel('Response Time (ms)', size=20)
    plt.tick_params(axis='x', labelsize=15)
    plt.tick_params(axis='y', labelsize=15)
    plt.plot(df.Percent, df.ResponseT, color='#CC4F1B',
                label='Response Time for Percentage of Bridges Blocked')
    plt.fill_between(df.Percent, df.ResponseT-df.ReTstdev,
                df.ResponseT+df.ReTstdev, alpha=0.5, edgecolor='#CC4F1B',
                facecolor='#FF9848')
    plt.tight_layout(pad=1)
    plt.savefig("Blockage-response-time.pdf")
    plt.close('all')

def set_plot_options():
      options = {
          'font.size': 12,
          'figure.figsize': (10,2),
          'figure.dpi': 100.0,
          'figure.subplot.left': 0.20,
          'figure.subplot.right': 0.97,
          'figure.subplot.bottom': 0.20,
          'figure.subplot.top': 0.90,
          'grid.color': '0.1',
          'grid.linestyle': ':',
          #'grid.linewidth': 0.5,
          'axes.grid' : True,
          #'axes.grid.axis' : 'y',
          #'axes.axisbelow': True,
          'axes.titlesize' : 25,
          'axes.labelsize' : 25,
          'axes.formatter.limits': (-4,4),
          'xtick.labelsize' : 30,#get_tick_font_size_10(),
          'ytick.labelsize' : 30,#get_tick_font_size_10(),
          'lines.linewidth' : 2.0,
          'lines.markeredgewidth' : 0.5,
          'lines.markersize' : 15,
      }

      for option_key in options:
          matplotlib.rcParams[option_key] = options[option_key]
      if 'figure.max_num_figures' in matplotlib.rcParams:
          matplotlib.rcParams['figure.max_num_figures'] = 100
      if 'figure.max_open_warning' in matplotlib.rcParams:
          matplotlib.rcParams['figure.max_open_warning'] = 100

if __name__ == "__main__":
    sys.exit(main())
