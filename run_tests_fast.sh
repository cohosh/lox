#!/bin/bash

cargo test --release --features=fast -- --nocapture stats_test_trust_levels > trust_levels.log
cargo test --release --features=fast -- --nocapture stats_test_invitations > invitations.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_05 > blockage_migration05.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_010 > blockage_migration010.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_20 > blockage_migration20.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_25 > blockage_migration25.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_30 > blockage_migration30.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_35 > blockage_migration35.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_40 > blockage_migration40.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_45 > blockage_migration45.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_50 > blockage_migration50.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_55 > blockage_migration55.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_60 > blockage_migration60.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_65 > blockage_migration65.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_70 > blockage_migration70.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_75 > blockage_migration75.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_80 > blockage_migration80.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_85 > blockage_migration85.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_90 > blockage_migration90.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_95 > blockage_migration95.log
cargo test --release --features=fast -- --nocapture stats_test_percent_blockage_migration_100 > blockage_migration100.log
echo "Completed all tests, now parsing results"
mv *.log Parsing-results
cd Parsing-results
./parse_data.sh
echo "Parse results are in the Parsing-results folder."

